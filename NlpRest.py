import requests
import json
import time
import glob
import os

class NlpRest:
    def __init__(self, user):
        self.USER = user
        self.URL = "http://ws.clarin-pl.eu/nlprest2/base"
        self.RESULTS_ENDPOINT = "/download"
        self.START_TASK_ENDPOINT = "/startTask/"
        self.STATUS_ENDPOINT = "/getStatus/"
        self.log_prefix = self.__class__.__name__ + ": "
    
    def upload(self, file):
        with open (file, "rb") as myfile:
            doc = myfile.read()
        res = requests.post(self.URL + '/upload/', data=doc, headers={'Content-Type': 'binary/octet-stream'})    
        return res.content.decode('utf-8')
    
    def process(self, data):
        doc = json.dumps(data)
        return requests.post(self.URL + '/startTask/', doc, {'Content-Type': 'application/json'}).content.decode('utf-8')

    def process_compressed(self, task, in_file):
        
        #global_time = time.time()
        fileid = self.upload(in_file)
    
        print(self.log_prefix + "Uploaded with id " + fileid)
        lpmn = 'filezip(' + fileid + ')|' + task
        data = {'lpmn': lpmn,'user': self.USER}
        return self.process(data)

    def get_result(self, taskid):
        
        print(self.log_prefix + "Task ID: " + taskid)
        resp = requests.get(self.URL + '/getStatus/' + taskid)
        
        data = json.loads(resp.content.decode('utf-8'))
        if data["status"] == "QUEUE" or data["status"] == "PROCESSING" :
            print(data["value"],end='\r')
            state = "PROCESSING"
        elif data["status"] == "ERROR":
            print(self.log_prefix + "Error " + data["value"])
            state = "ERROR"
        else:
            state = "SUCCESS"
            f = data["value"][0]["fileID"]
            content = requests.get(self.URL + '/download' + f + "/weighted.json")
            return {"result": state, "data": content.json()}

        return {"result": state, "data": data["value"]}

    def blocking_sync_execute_lpmn_pipeline(self, lpmn, text=""):
        data={}
        data['user'] = self.USER 
        data['lpmn'] = lpmn
        
        if(text != ""):
            data['text'] = text
       
        try:
            r = requests.post(self.URL + self.START_TASK_ENDPOINT, json.dumps(data), headers={'Content-Type': 'application/json'})
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err)
            return
        
        task_id = r.text
        print(self.log_prefix + 'Got task_id = ' + task_id)

        file_id = self._wait_for_finish(task_id)
        if(file_id == ""):
            print("Error: no processing not finished")
            return

        results = self._get_results(file_id) 
        if(results == ""):
            print("Error: can't get results")
            return

        return results 

    def _wait_for_finish(self, task_id):
        # TODO: timeout
        time.sleep(0.05)

        while True:
            time.sleep(0.01)
            r = requests.get(self.URL + self.STATUS_ENDPOINT + task_id)
            data = r.json()

            if(data["status"] == "QUEUE" or data["status"] == "PROCESSING"):
                continue
            elif data["status"] == "ERROR":
                print("Error "+data["value"])
                return ""
            else:
                file_id = data["value"][0]["fileID"]
                return file_id # TODO: check for successfull status

    def _get_results(self, file_id):
        r = requests.get(self.URL + self.RESULTS_ENDPOINT + file_id)
        return r.text

if __name__ == "__main__":
    nlprest = NlpRest("user")
    task = 'any2txt|wcrft2|mwe|converter({"type":"ccl2vec","tagfilter":["noun"]})|dir|feature2({"type":"word2vec"})'
    r = nlprest.process_compressed(task, "korpusy/tematy_very_short.zip")
    
    vec_words = []
    for vec in r["arr"]:
        vec_words.append(vec)
    
    print(vec_words)