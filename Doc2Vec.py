import Keyword_extractor
from Word2vec import Word2Vec
from numpy import median
from DimensionReducer import decrease_dimensions
from CorpusLoader import CorpusLoader
from KeywordsExtractor import KeywordExtractor
from NlpRest import NlpRest

class VectorsReducer:
    """Creates one vector from many vectors describing a document"""
    def __init__(self):
        pass

    def get_vec(self, vec_words):
        return median(vec_words, axis=0)

class Doc2Vec:
    def __init__(self, nlpRest="", keywordExtractor=KeywordExtractor, word2Vec=Word2Vec, vectorsReducer=VectorsReducer):
        self.keywordExtractor = keywordExtractor(nlpRest)
        self.word2Vec = word2Vec()
        self.vectorsReducer = vectorsReducer()
        self.log_prefix = self.__class__.__name__ + ": "

    def get_vec(self, text):
        keywords = self.keywordExtractor.extract(text)
        vec_words = self.word2Vec.get_vec_of_words(keywords)
        vecs = []
        for w in vec_words:
            vecs.append(w[1])

        return self.vectorsReducer.get_vec(vecs)

if __name__ == "__main__":

    cLoader = CorpusLoader()
    doc2Vec = Doc2Vec(NlpRest("user"))
    
    documents = cLoader.get_documents("text_files")
    doc_vecs = []
    for d in documents:
        doc_vecs.append(doc2Vec.get_vec(d.as_string()))
   
    cords = decrease_dimensions(doc_vecs, 2)
    print(cords)
