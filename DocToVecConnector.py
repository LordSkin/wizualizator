import os

import numpy
from Doc2VecClarin import Doc2VecClarin

from CorpusLoader import CorpusLoader
from Doc2Vec import Doc2Vec
from NlpRest import NlpRest


def get_vector(corpus_path):
    """
    Funkacja łącząca z DocToVec Api i
    :param file: nieprzetworzony plik z zapytania
    :return: lista zawierajaca nazwy plikow oraz wektor cech zwrócony z DocToVecApi
    """
    doc2Vec = Doc2VecClarin(NlpRest("user"))
    return doc2Vec.get_vec_from_corps(corpus_path)

def get_result(taskid):
    doc2Vec = Doc2VecClarin(NlpRest("user"))
    return doc2Vec.get_result(taskid)

def _convert_file(file):
    """
    funkcja konwertująca plik do formatu wymaganego przez DOCTOVEC api
    :return:
    """
    return numpy.empty([1, 1])


def _get_vector(korpus):
    """
    Funkcja pobiera wektor cech z DocToget_vectorec
    :param korpus:
    :return:
    """
    return [[1, 2, 3], [4, 2, 1], [2, 56, 5], [0, 1, -2], [0, 0, 0], [1, 2, 353], [4, 2, 6]]
