import json
import os
import time

import numpy
from flask import Flask, request, flash
from flask_cors import CORS

from DimensionReducer import decrease_dimensions
from DocToVecConnector import get_result
from DocToVecConnector import get_vector
from File_with_progress import File_with_progress
from Text_with_position import Text_with_position

KORPUS = 'korpus'
TMP_KORPUS_DIR = "_runtime_korpusy/"
app = Flask(__name__)
app.secret_key = "super secret key"
cors = CORS(app, resources={r"/*": {"origins": "*"}})
korpus_progresses = {}


def _create_pojo_object(names, final_vector):
    points = []
    name_to_color = _get_names_to_color(names)
    i = 0
    if len(final_vector.shape) < 2:
        points.append(Text_with_position(names[0], final_vector, name_to_color[_get_name_prefix(names[0])]).__dict__)
    else:
        for name in names:
            points.append(Text_with_position(name, final_vector[i, :], name_to_color[_get_name_prefix(name)]).__dict__)
            i = i + 1
    return points


def _create_pojo_progress():
    results = []
    for key in korpus_progresses:
        results.append(File_with_progress(key, korpus_progresses[key]))

    return results


def _get_names_to_color(names):
    dictionary = {}
    for name in names:
        name_prefix = _get_name_prefix(name)
        if name_prefix not in dictionary:
            dictionary[name_prefix] = len(dictionary)
    return dictionary


def _get_name_prefix(name):
    pos = name.index("_")
    if pos <= 0:
        return name
    else:
        return name[:pos]


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


@app.route('/progress/', methods=['GET'])
def get_progress():
    return json.dumps(_create_pojo_progress(), default=lambda o: o.__dict__,
            sort_keys=True, indent=4)


@app.route('/', methods=['POST'])
def get_dimensions():
    dimensions = request.args.get("dimensions")
    if not dimensions:
        dimensions = 2

    if not os.path.exists(TMP_KORPUS_DIR):
        os.makedirs(TMP_KORPUS_DIR)

    if KORPUS not in request.files:
        flash('No file part')
        return "No korpus file", 400
    else:
        file = request.files[KORPUS]
        korpus_path = os.path.basename(TMP_KORPUS_DIR + file.filename)
        file.save(korpus_path)
        #        extracted_korpus = os.path.splitext(korpus_path)[0]
        #        with ZipFile(korpus_path, 'r') as zipObj:
        #            zipObj.extractall(extracted_korpus)

        taskID = get_vector(korpus_path)
        while True:
            time.sleep(0.5)
            resp = get_result(taskID)
            state = resp['result']
            if state == "PROCESSING":
                print(
                    "Done " + str(resp['data']))  # Tutaj jest wartosc, ktora mowi ile jeszcze zostalo do przetworzenia
                global korpus_progresses
                if len(korpus_progresses) > 2000:
                    korpus_progresses = {}
                korpus_progresses[file.filename] = resp['data']
            elif state == "SUCCESS":
                [names, vector] = resp['data']
                break

        methods = ['isomap', 'locallyLinearEmbedding', 'MDS', 'spectralEmbedding', 'TSNE']
        dictionary = {}
        for method in methods:
            dictionary[method] = _create_pojo_object(names, decrease_dimensions(vector, int(dimensions), method))

        # final_vector = decrease_dimensions(vector, int(dimensions), method)
        #        shutil.rmtree(extracted_korpus)
        os.remove(korpus_path)

        # return json.dumps(_create_pojo_object(names, final_vector), cls=NumpyEncoder)
        return json.dumps(dictionary, cls=NumpyEncoder)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=os.getenv('PORT', 5000))
