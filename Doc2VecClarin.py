import json
import requests
import glob
import os
import time

from numpy import median
from CorpusLoader import CorpusLoader
from NlpRest import NlpRest
from DimensionReducer import decrease_dimensions

class Doc2VecClarin:
    def __init__(self, nlpRest=""):
        self.nlpRest = nlpRest
        self.log_prefix = self.__class__.__name__ + ": "

    def get_vec_from_corps(self, corps):
        #with document division into smaller parts - 20 kbytes
        task = 'any2txt|div(200000)|wcrft2|mwe|converter({"type":"ccl2vec","tagfilter":["noun"]})|dir|feature2({"type":"word2vec"})'
        #task = 'any2txt|wcrft2|mwe|converter({"type":"ccl2vec","tagfilter":["noun"]})|dir|feature2({"type":"word2vec"})'
        return self.nlpRest.process_compressed(task, corps)

    def get_result(self, taskid):

        result = self.nlpRest.get_result(taskid)
        r =  result["result"]
        if r == "SUCCESS":
            titles = []
            vec_words = []
            for vec in result["data"]["arr"]:
                vec_words.append(vec)

            for title in result["data"]["rowlabels"]:
                titles.append(title)

            data = [titles, vec_words]
        else:
           data = result["data"] 
        
        return {"result": r, "data": data}

if __name__ == "__main__":
    doc2Vec = Doc2VecClarin(NlpRest("user"))
    vec = doc2Vec.get_vec_from_corps("korpusy/tematy.zip")
    print(vec)
    cords = decrease_dimensions(vec, 2)
    print(cords)