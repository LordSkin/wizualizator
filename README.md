# Instalacja i uruchomienie
## Wersja Pythona: **3.6**
## Instalacja dodatkowych pakietów(wymaga uprawnień administratora)
```
pip install -r requirements.txt
```
## Uruchamianie
```
python .\Rest_Controller.py
```


# REST API do wizualizacji
## 1. Pobranie wektora cech na podstawie korpusu

### Parametry wejściowe
* Metoda POST
* parametry w ścieżce: 
    * dimensions - liczba wymiarów do jakiej sprowadzamy podobienstwo
* parametry w BODY:
    * korpus - plik z korpusem słów (format do dogadania)

### Przykład:
```
localhost:5000/?dimensions=2
```

### Parametry wyjściowe
* Słownik: **nazwa metody** ['isomap', 'locallyLinearEmbedding', 'MDS', 'spectralEmbedding', 'TSNE'] -> **lista punktów** (punkt zawiera współrzędne, nazwę pliku oraz numer koloru)

### Przykład
```json
{
  "isomap": [
    {
      "file_name": "muzyka_2_4",
      "position": [
        -0.4249106646042632,
        -0.30394737132213173
      ],
      "color": 0
    },
    {
      "file_name": "prawo_1_1",
      "position": [
        0.7969388798597917,
        -0.10244230365466822
      ],
      "color": 1
    },
    {
      "file_name": "muzyka_2_3",
      "position": [
        -0.43633593351830124,
        -0.30956812533420436
      ],
      "color": 0
    }
  ],
  "locallyLinearEmbedding": [
    {
      "file_name": "muzyka_2_4",
      "position": [
        -0.03877008362564653,
        -0.25649759383515824
      ],
      "color": 0
    },
    {
      "file_name": "prawo_1_1",
      "position": [
        0.6904943003426697,
        -0.0040499751286276475
      ],
      "color": 1
    },
    {
      "file_name": "muzyka_2_3",
      "position": [
        -0.03876935105390543,
        -0.25638256696400064
      ],
      "color": 0
    }
  ],
  ...
}
```

## Pobranie progresu przetwarzania
### Parametry wejściowe
*ścieżka /progress/
* Metoda GET
### Przykład:
```
localhost:5000/progress/
```
### Parametry wyjściowe
* Lista plików wraz z progresem

### Przykład
```
[ 
   { 
      "file_name":"5_autorow.zip",
      "progress":0.16901408450704225
   },
   { 
      "file_name":"korpus2.zip",
      "progress":0.5
   }
]
```

## BUGS

### Progress
Jeżeli słownik osiągnie 2000 pozycji automatycznie się czyści, niezależnie od wartości progresu. Jest to jedyny sposób na usunięcei  niego pozycji

### Word2Vec
Dla słowa "grożący" otrzymujemy pustą odpowiedź od word2vec

Zapytanie:
```result = requests.post(url, json={"task":"all","lexeme":words,"tool":"word2vec"})```

Odpowiedź:
```{'input': {'task': 'all', 'lexeme': ['grożący'], 'tool': 'word2vec'}, 'totaltime': 0.015, 'error': '', 'results': {'grożący': {'vector': [], 'kbest': []}}}```

