FROM python:3.7.5-buster

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt
EXPOSE 5000

CMD ["python", "Rest_Controller.py"]