from NlpRest import NlpRest
import argparse

class KeywordExtractor:
    def __init__(self, nlpRest):
        self.nlprest = nlpRest

    def extract(self, text):

        lpmn = 'any2txt|wcrft2|respa'
        r = self.nlprest.blocking_sync_execute_lpmn_pipeline(lpmn, text)
        lines = r.split('\n')
        keywords = []
        for l in lines:
            elements = l.split(' ')
            elements.pop() # last element is a number
            keywords = keywords + elements 
        
        return keywords 

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extract keywords from text')
    parser.add_argument("--u", type=str, required=True, help="user name for nlprest")
    parser.add_argument("--f", type=str, required=True, help="path to text file")

    args = parser.parse_args()
    user = args.u
    path = args.f

    keywordExtractor = KeywordExtractor(NlpRest(user))
    print(keywordExtractor.extract(open(path, "r").read()))