import requests 
from statistics import mean

url = "http://ws.clarin-pl.eu/lexrest/lex/" 

class Word2Vec:
    def __init__(self):
        self.log_prefix = self.__class__.__name__ + ": "

    def get_vec_of_words(self, words):
        """Replace list of words to list of vectors
        In: list of words
        Out: [word, [float, float, ...]], [word, [float, float, ...]], ...]
        """ 
        
        result = requests.post(url, json={"task":"all","lexeme":words,"tool":"word2vec"})
        data = result.json()
        vec_words = []
        for w in words:
            try: 
                vec = [w, data["results"][w]["vector"][0][1]]
                vec_words.append(vec)
            except:
                print(self.log_prefix + "No results for word:" + w)
        
        return vec_words

if __name__ == "__main__":

    word2Vec = Word2Vec() 
    words = ["grożący"]
    vec_words = word2Vec.get_vec_of_words(words)
    for w in vec_words:
        print("Word: " + w[0])
        print("Num of vectors: " + str(len(w[1])))
        print("Average: " + str(mean(w[1])) + '\n')

    pass