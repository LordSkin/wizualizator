import os
from Document import Document

class CorpusLoader:
    def __init__(self):
        pass

    def get_documents(self, path):
        documents = []
        for f in os.listdir(path):
            documents.append(Document(path + '/' + f))

        return documents
