import numpy
from sklearn.manifold import Isomap, LocallyLinearEmbedding, MDS, SpectralEmbedding, TSNE


def decrease_dimensions(vector, dimensions, method='isomap'):
    """
    Funkcja zmniejszająca liczbę wymiarów wektora za pomocą jakiegośtam algorytmu
    :param method: Algorytm używany do redukcji wymiarowości [isomap, locallyLinearEmbedding, MDS, spectralEmbedding, TSNE]
    :param vector: wekto wielowymiarowy do zmniejszenia
    :param dimensions: liczba wymiarów do której zmniejszamy zadany wektor
    :return:
    """

    vector = numpy.array(vector)
    if vector.shape[0] < 2:
        return numpy.zeros(dimensions, dtype=int)

    if method == 'isomap':
        embedding = Isomap(n_components=dimensions, n_jobs=-1, n_neighbors=_get_neighbours_count(vector)-1)
    elif method == 'locallyLinearEmbedding':
        embedding = LocallyLinearEmbedding(n_components=dimensions, n_jobs=-1)
    elif method == 'MDS':
        embedding = MDS(n_components=dimensions, n_jobs=-1)
    elif method == 'spectralEmbedding':
        embedding = SpectralEmbedding(n_components=dimensions, n_jobs=-1)
    elif method == 'TSNE':
        embedding = TSNE(n_components=dimensions, n_jobs=-1)
    else:
        embedding = Isomap(n_components=dimensions, n_neighbors=_get_neighbours_count(vector)-1, n_jobs=-1)

    return embedding.fit_transform(vector)


def _get_neighbours_count(vector):
    return min(6, vector.shape[0])
