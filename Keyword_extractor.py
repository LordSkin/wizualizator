from rake_nltk import Rake

stop_word_file_eng = "stop_words_eng.txt"
stop_word_file_pl = "stop_words_pl.txt"

r = Rake(stopwords=stop_word_file_pl, max_length=1)  # Uses stopwords for polish from NLTK, and all puntuation characters.


def _extract_keywords_from_text(document):
    """
    Funkcja zamieniająca dokument w listę słów kluczowych posortowaną od najbardziej kluczowego
    :param document: String z treścią dokumentu
    :return: Tablica słów kluczowych posortowanych od najwazniejszego
    """
    r.extract_keywords_from_text(document)
    return r.get_ranked_phrases()
