class Document:
    def __init__(self, path):
        self.name = path
        self.file = open(path, "r", encoding="utf-8")

    def as_string(self):
        self.file.seek(0,0)
        return self.file.read()

    def get_name(self):
        return self.name
    
    def __del__(self):
        self.file.close()